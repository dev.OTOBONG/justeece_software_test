# Necessary imports for selenium python bindings.
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time


class JusteeceSoftwareApp(unittest.TestCase):
    """ A class that models test scenarios for justeece software application. """

    @classmethod
    def setUpClass(cls):
        """ Defines basic set ups for test case scenarios. """

        # Creates a web driver object.
        serv = Service(r"/home/machines/webdrivers/chromedriver")
        cls.driver = webdriver.Chrome(service=serv)

        # Waits for 30 seconds for web elements to be available before proceeding.
        cls.driver.implicitly_wait(30)

        # Opens the url; https://dev2.justeece.com
        cls.driver.get("https://dev2.justeece.com")

        # Maximize the web page
        cls.driver.maximize_window()

        # Pauses browser for 10 seconds before proceeding.
        time.sleep(10)

    def test_SignupTest(self):
        """ A test scenario that verifies user signup. """

        # Locates and clicks the signup button.
        signUpBtn = self.driver.find_element(By.CSS_SELECTOR, "a.btn:nth-child(2)").click()

        # Automates verification for sign up details.
        # first name verification.
        firstNameField = self.driver.find_element(By.ID, "fname")

        if firstNameField.is_displayed() and firstNameField.is_enabled():
            firstNameField.send_keys("Tester")

        # Last name verification
        lastNameField = self.driver.find_element(By.ID, "lname")

        if lastNameField.is_displayed() and lastNameField.is_enabled():
            lastNameField.send_keys("Otison")

        # Email field verification
        emailField = self.driver.find_element(By.ID, "femail")

        if emailField.is_displayed() and emailField.is_enabled():
            emailField.send_keys("testerjusteece@gmail.com")

        # Password verification
        passwordField = self.driver.find_element(By.ID, "fpassword")

        if passwordField.is_displayed() and passwordField.is_enabled():
            passwordField.send_keys("Justtester100..?")

        # Select checkbox
        checkBox = self.driver.find_element(By.CSS_SELECTOR, ".terms-condition > label:nth-child(2)").click()

        status_checkbox = self.driver.find_element(By.CSS_SELECTOR, ".terms-condition > label:nth-child(2)")
        if status_checkbox.is_selected():
            pass

        # Simulates a keyboard end key press to go to the end of the page.
        html = self.driver.find_element(By.TAG_NAME, "html").send_keys(Keys.END)

        # Click account creation button.
        createAccountBtn = self.driver.find_element(By.CSS_SELECTOR, "button.btn:nth-child(10)")
        createAccountBtn.click()

        # Simulates a keyboard end key press to go to the end of the page.
        html = self.driver.find_element(By.TAG_NAME, "html").send_keys(Keys.END)

    @classmethod
    def tearDownClass(cls):

        # Pauses browser for 10 seconds.
        time.sleep(10)

        # Close browser window.
        cls.driver.close()


if __name__ == "__main":
    unittest.main()






